import { Component , ViewChild} from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { HomePage } from '../home/home';
import * as WC from 'woocommerce-api';
import { ProductsByCategory } from '../products-by-category/products-by-category'

@Component({
  selector: 'page-menu',
  templateUrl: 'menu.html',
})
export class Menu {

  homePage: any;
  WooCommerce: any;
  categories: any[];
  @ViewChild('content') childNavCtrl: NavController;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  this.homePage =HomePage;
  this.categories=[];
      
  this.WooCommerce= WC ({
    url: "http://localhost/dashboard/",
    consumerKey: "ck_d65b4ead099e4f2bbbd51118288e00bc3a1051ef",
    consumerSecret: "cs_ba227199bf57853f7188429438d15274d31b08dc", //Your consumer secret
   
  });

  this.WooCommerce.getAsync("products/categories").then(
    (data)=>{console.log(JSON.parse(data.body).product_categories);

      let temp:any[] = JSON.parse(data.body).product_categories;
   
      for(let i=0; i<temp.length;i++){
        if(temp[i].parent==0){


          if(temp[i].slug== "clothing"){
            temp[i].icon="shirt";
          }
          if(temp[i].slug== "music"){
            temp[i].icon="musical-notes";
          }
          if(temp[i].slug== "decor"){
            temp[i].icon="cog";
          }
          if(temp[i].slug== "uncategorized"){
            temp[i].icon="close";
          }

          this.categories.push(temp[i]);
        }
      }
    },(err)=>{console.log(err) }
    )
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad Menu');
  }

  openCategoryPage(category){
    this.childNavCtrl.setRoot(ProductsByCategory,{"category": category});
  }

}
