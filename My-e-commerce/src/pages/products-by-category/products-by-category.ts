import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import * as WC from 'woocommerce-api';



@Component({
  selector: 'page-products-by-category',
  templateUrl: 'products-by-category.html',
})
export class ProductsByCategory {

  WooCommerce: any;
  products:any[];
  page: number;
  category: any;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
 
    this.page=1;
    this.category=this.navParams.get("category");
 
    this.WooCommerce= WC ({
      url: "http://localhost/dashboard/",
      consumerKey: "ck_d65b4ead099e4f2bbbd51118288e00bc3a1051ef",
      consumerSecret: "cs_ba227199bf57853f7188429438d15274d31b08dc", //Your consumer secret
     
    });
 
    this.WooCommerce.getAsync("products?filter[category]=" + this.category.slug)
    .then(
      (data)=>{console.log(JSON.parse(data.body));
      this.products=JSON.parse(data.body).products;
     },
      (err)=>{console.log(err) }
      )


  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ProductsByCategoryPage');
  }
  
  loadMoreProducts(event){

  this.page ++;
  console.log("Getting page " + this.page);
  
    this.WooCommerce.getAsync("products?filter[category]=" +this.category.slug + "&page=" + this.page)
    .then(
      (data)=>{
        let temp = (JSON.parse(data.body).products);

      this.products=this.products.concat(JSON.parse(data.body).products)
      console.log(this.products);
      event.complete();
       
      if(temp.legnth <10)
         event.enable(false);
      }
      )
   }
  

}
