import { Component, ViewChild } from '@angular/core';
import { NavController, Slides, ToastController } from 'ionic-angular';
import * as WC from 'woocommerce-api';
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  
  moreProducts : any[];
  Woocommerce: any;
  products: any[];
  page:number;
  

@ViewChild('productSlides') productSlides : Slides;

  constructor(public navCtrl: NavController, public toastCtrl :ToastController) {

this.page = 2;

   this.Woocommerce= WC ({
     url: "http://localhost/dashboard/",
     consumerKey: "ck_d65b4ead099e4f2bbbd51118288e00bc3a1051ef",
     consumerSecret: "cs_ba227199bf57853f7188429438d15274d31b08dc", //Your consumer secret
    
   });
this.loadMoreProducts(null);

   this.Woocommerce.getAsync("products").then(
     (data)=>{console.log(JSON.parse(data.body));
     this.products=JSON.parse(data.body).products;
    },
     (err)=>{console.log(err); }
     )
  }

ionViewDidLoad(){
  setInterval(()=>{
    if(this.productSlides.getActiveIndex()==this.productSlides.length()-1)
       this.productSlides.slideTo(0);
   this.productSlides.slideNext();
  },3000)
}


loadMoreProducts(event){

  if(event ==null)
  {
     this.page=2;
     this.moreProducts = [];
  }
  else
     this.page ++;

  this.Woocommerce.getAsync("products?page=" +this.page).then(
    (data)=>{console.log(JSON.parse(data.body));
    this.moreProducts=this.moreProducts.concat(JSON.parse(data.body).products);

    if(event!=null)
    {
      event.complete();
    }
    if(JSON.parse(data.body).products.legnth <10)
       event.enable(false);

       this.toastCtrl.create({
       message:"No more products!",
       duration:5000
      }).present();
   },
    (err)=>{console.log(err); }
    )
 }
}
