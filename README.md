# ZeynabaDEME_M2FIC_VTI_Ionic

I have build an eCommerce application (IONIC 3)  that will allow the user to browse through the product catalog and product category. 
The data are provided by WOocommerce(https://fr.wordpress.org/plugins/woocommerce/) for free and I set it up in a Wordpress 
application.

Once the user gets in the application, he/she can browse though the product categories and he can choose a category in
order to show all the products related to that category. He can also get back to the menu.
